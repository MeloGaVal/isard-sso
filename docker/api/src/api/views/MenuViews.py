#!flask/bin/python
# coding=utf-8
from api import app
import logging as log
import traceback

from uuid import uuid4
import time,json
import sys,os
from flask import render_template, Response, request, redirect, url_for, jsonify

from ..lib.menu import Menu
menu = Menu()

@app.route('/header/<format>', methods=['GET'])
@app.route('/header/<format>/<application>', methods=['GET'])
def api_v2_header(format,application=False):
    if application == False:
        if format == 'json':
            if application == False:
                return json.dumps(menu.get_header()), 200, {'Content-Type': 'application/json'}
        if format == 'html':
            if application == False:
                return render_template('header.html')
            if application == 'nextcloud':
                return render_template('header_nextcloud.html')
            if application == 'wordpress':
                return render_template('header_wordpress.html')
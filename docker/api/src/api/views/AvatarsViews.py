#!flask/bin/python
# coding=utf-8
from api import app
import logging as log
import traceback

from uuid import uuid4
import time,json
import sys,os
from flask import render_template, Response, request, redirect, url_for, jsonify, send_from_directory

from ..lib.avatars import Avatars
avatars = Avatars()

@app.route("/avatar/<username>", methods=["GET"])
def avatar(username):
    print(app.root_path)
    log.error(app.root_path)
    return send_from_directory(os.path.join(app.root_path, '../avatars/master-avatars/'), avatars.get_user_avatar(username), mimetype='image/jpg')

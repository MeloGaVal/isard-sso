#!/usr/bin/env python
# coding=utf-8
# Copyright 2017 the Isard-vdi project authors:
#      Josep Maria Viñolas Auquer
#      Alberto Larraz Dalmases
# License: AGPLv3
import time
from api import app as application
from datetime import datetime, timedelta
import pprint

import logging
import traceback
import yaml, json

from jinja2 import Environment, FileSystemLoader


def write_css():
    env = Environment(loader=FileSystemLoader('api/static/_templates'))
    css_template = env.get_template('dd.css')
    with open('menu/custom.yaml', 'r') as menu_custom_file:
        menu_custom_yaml = menu_custom_file.read()
    menu_custom = yaml.full_load(menu_custom_yaml)
    css = css_template.render(data=menu_custom)
    with open("api/static/css/dd.css", "w") as css_file:
        css_file.write(css)


class Menu():
    def __init__(self):
        self.menudict=self.gen_header()
        pprint.pprint(self.menudict)
        self.write_headers()
        write_css()

    def gen_header(self):
        with open(r'menu/system.yaml') as yml:
            system=yaml.load(yml, Loader=yaml.FullLoader)
        apps_internal = []
        for app in system['apps_internal']:
            app['href']='https://'+app['subdomain']+'.'+application.config['DOMAIN']+app['href']
            del app['subdomain']
            apps_internal.append(app)

        with open(r'menu/custom.yaml') as yml:
            custom=yaml.load(yml, Loader=yaml.FullLoader)
        custom['background_login']='https://api.'+application.config['DOMAIN']+custom['background_login']
        custom['logo']='https://api.'+application.config['DOMAIN']+custom['logo']

        menudict={**custom,**{'apps_internal':apps_internal}}
        menudict['user']={}
        menudict['user']['account']='https://sso.'+application.config['DOMAIN']+system['user']['account']
        menudict['user']['avatar']='https://sso.'+application.config['DOMAIN']+system['user']['avatar']
        menudict['user']['password']='https://sso.'+application.config['DOMAIN']+system['user']['password']
        return menudict

    def write_headers(self):
        env = Environment(loader=FileSystemLoader('api/static/_templates'))
        template = env.get_template('apps.html')
        output_from_parsed_template = template.render(data=self.menudict)
        print(output_from_parsed_template)
        with open("api/static/templates/header.html", "w") as fh:
            fh.write(output_from_parsed_template)

        with open("api/static/templates/header_nextcloud.html", "w") as fh:
            fh.write(output_from_parsed_template)
        with open("api/static/templates/header_nextcloud.html", "a") as fh:
            with open("api/static/_templates/nextcloud.html", "r") as nextcloud:
                fh.write(nextcloud.read())
        with open("api/static/templates/header.json", "w") as fh:
            fh.write(json.dumps(self.menudict))

    def get_header(self):
        return self.menudict
        # with open('menu.yaml', 'w') as yml:
        #     print(yaml.dump(header, yml, allow_unicode=True))

#!/bin/sh
#set -e
# https://spoore.wordpress.com/2017/02/21/how-to-setup-keycloi/
ipa-client-install \
     --fixed-primary \
     --server ipa.$DOMAIN \
     --domain $DOMAIN \
     --principal admin \
     --password freeipafreeipa \
     --unattended \
     --no-nisdomain \
     --force-join \
     -N  # No NTP now   

echo freeipafreeipa|kinit admin
ipa service-add HTTP/sso.santantoni.duckdns.org@SANTANTONI.DUCKDNS.ORG
ipa-getkeytab -s ipa.santantoni.duckdns.org \
     -p HTTP/sso.$DOMAIN@$(echo "$DOMAIN" | awk '{ print toupper($0) }') \
     -k /etc/ipa.keytab
echo "Adding admin user"
#/usr/sbin/sssd -i -d 4 &
#/opt/keycloak/keycloak-12.0.4/bin/federation-ssd-setup.sh
/opt/keycloak/keycloak-12.0.4/bin/add-user-keycloak.sh -r master -u admin -p $KK_ADMIN_PWD
echo "Starting keycloak"
/opt/keycloak/keycloak-12.0.4/bin/standalone.sh -b 0.0.0.0


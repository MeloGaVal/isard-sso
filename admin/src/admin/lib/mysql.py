#!/usr/bin/env python
# coding=utf-8
import time
from admin import app
from datetime import datetime, timedelta

import logging as log
import traceback
import yaml, json

import mysql.connector

class Mysql():

    def __init__(self,host,database,user,password):
        self.conn = mysql.connector.connect(
            host=host,
            database=database,
            user=user,
            password=password)

    def select(self,sql):
        self.cur = self.conn.cursor()
        self.cur.execute(sql)
        data=self.cur.fetchall()
        self.cur.close()
        return data

    def update(self,sql):
        self.cur = self.conn.cursor()
        self.cur.execute(sql)
        self.conn.commit()
        self.cur.close()

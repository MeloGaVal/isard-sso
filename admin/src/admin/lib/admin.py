from admin import app

from .keycloak_client import KeycloakClient
from .moodle import Moodle
from .nextcloud import Nextcloud
from .nextcloud_exc import ProviderItemExists
from .avatars import Avatars

from .helpers import filter_roles_list, filter_roles_listofdicts

import logging as log
from pprint import pprint
import traceback, os, json
from time import sleep

import diceware
options = diceware.handle_options(None)
options.wordlist = 'cat_ascii'
options.num = 3

from .events import Events

class Admin():
    def __init__(self):
        ready=False
        while not ready:
            try:
                self.keycloak=KeycloakClient(verify=app.config['VERIFY'])
                ready=True
            except:
                log.error(traceback.format_exc())
                log.error('Could not connect to keycloak, waiting to be online...')
                sleep(2)
        log.warning('Keycloak connected.')

        ready=False
        while not ready:
            try:
                self.moodle=Moodle(verify=app.config['VERIFY'])
                ready=True
            except:
                log.error('Could not connect to moodle, waiting to be online...')
                sleep(2)
        log.warning('Moodle connected.')

        ready=False
        while not ready:
            try:
                with open(os.path.join(app.root_path, "../moodledata/saml2/moodle."+app.config['DOMAIN']+".pem"),"r") as pem:
                    ready=True
            except IOError:
                log.warning('Could not get moodle SAML2 pem certificate. Retrying...')
                sleep(2)

        ready=False
        while not ready:
            try:
                self.nextcloud=Nextcloud(verify=app.config['VERIFY'])
                ready=True
            except:
                log.error('Could not connect to nextcloud, waiting to be online...')
                sleep(2)
        log.warning('Nextcloud connected.')

        self.default_setup()
        self.internal={}

        ready=False
        while not ready:
            try:
                self.resync_data()
                ready=True
            except:
                log.error('Could not connect to moodle, waiting to be online...')
                sleep(2)

        self.external={'users':[],
                        'groups':[],
                        'roles':[]}
        log.warning(' Updating missing user avatars with defaults')
        self.av=Avatars()
        # av.minio_delete_all_objects() # This will reset all avatars on usres
        self.av.update_missing_avatars(self.internal['users'])
        log.warning(' SYSTEM READY TO HANDLE CONNECTIONS')

    ## This function should be moved to postup.py
    def default_setup(self):
        log.warning('Setting defaults...')
        dduser=os.environ['DDADMIN_USER']
        ddpassword=os.environ['DDADMIN_PASSWORD']
        ddmail=os.environ['DDADMIN_EMAIL']

        ### User admin in group admin
        try:
            log.warning('KEYCLOAK: Adding group admin and user admin to this group')
            self.keycloak.add_group('admin')
            ## Add default admin user to group admin (for nextcloud, just in case we go there)
            admin_uid=self.keycloak_admin.get_user_id('admin')
            self.keycloak_admin.group_user_add(admin_uid,gid)
            log.warning('KEYCLOAK: OK')
        except:
            # print(traceback.format_exc())
            log.warning('KEYCLOAK: Seems to be there already')

        #### Add default groups
        try:
            log.warning('KEYCLOAK: Adding default groups')
            self.keycloak.add_group('manager')
            self.keycloak.add_group('teacher')
            self.keycloak.add_group('student')
            log.warning('KEYCLOAK: OK')
        except:
            log.warning('KEYCLOAK: Seems to be there already')

        try:
            log.warning('MOODLE: Adding default group admin')
            self.moodle.add_system_cohort('/admin','system admins')
            log.warning('MOODLE: OK')
        except:
            log.warning('MOODLE: Seems to be there already')

        try:
            log.warning('MOODLE: Adding default group manager')
            self.moodle.add_system_cohort('/manager','system managers')
            log.warning('MOODLE: OK')
        except:
            log.warning('MOODLE: Seems to be there already')

        try:
            log.warning('MOODLE: Adding default group teacher')
            self.moodle.add_system_cohort('/teacher','system teacher')
            log.warning('MOODLE: OK')
        except:
            log.warning('MOODLE: Seems to be there already')

        try:
            log.warning('MOODLE: Adding default group student')
            self.moodle.add_system_cohort('/student','system student')
            log.warning('MOODLE: OK')
        except:
            log.warning('MOODLE: Seems to be there already')

        try:
            log.warning('NEXTCLOUD: Adding default group admin')
            self.nextcloud.add_group('/admin')
            log.warning('NEXTCLOUD: OK')
        except ProviderItemExists:
            log.warning('NEXTCLOUD: Seems to be there already')

        try:
            log.warning('NEXTCLOUD: Adding default group manager')
            self.nextcloud.add_group('/manager')
            log.warning('NEXTCLOUD: OK')
        except ProviderItemExists:
            log.warning('NEXTCLOUD: Seems to be there already')

        try:
            log.warning('NEXTCLOUD: Adding default group teacher')
            self.nextcloud.add_group('/teacher')
            log.warning('NEXTCLOUD: OK')
        except ProviderItemExists:
            log.warning('NEXTCLOUD: Seems to be there already')

        try:
            log.warning('NEXTCLOUD: Adding default group student')
            self.nextcloud.add_group('/student')
            log.warning('NEXTCLOUD: OK')
        except ProviderItemExists:
            log.warning('NEXTCLOUD: Seems to be there already')


        ### Add default roles
        try:
            log.warning('KEYCLOAK: Adding default roles')
            self.keycloak.add_role('manager','Realm managers')
            self.keycloak.add_role('teacher','Realm teachers')
            self.keycloak.add_role('student','Realm students')
            log.warning('KEYCLOAK: OK')
        except:
            log.warning('KEYCLOAK: Seems to be there already')


        ### ddadmin user
        try:
            log.warning('KEYCLOAK: Adding user ddadmin and adding to group and role admin')
            ## Assign group admin to this dduser for nextcloud
            uid=self.keycloak.add_user(dduser,'DD','Admin',ddmail,ddpassword,group='admin')
            ## Assign role admin to this user for keycloak, moodle and wordpress
            self.keycloak.assign_realm_roles(uid,'admin')
            log.warning('KEYCLOAK: OK')
        except:
            log.warning('KEYCLOAK: Seems to be there already')


        try:
            log.warning('KEYCLOAK: Adding default users system_teacher, system_manager and system_student users')
            uid=self.keycloak.add_user('system_manager','Manager','System','fakemanager@fake.com','m@n@g3r',group='manager')
            self.keycloak.assign_realm_roles(uid,'manager')
            uid=self.keycloak.add_user('system_teacher','Teacher','System','faketeacher@fake.com','t3@ch3r',group='teacher')
            self.keycloak.assign_realm_roles(uid,'teacher')
            uid=self.keycloak.add_user('system_student','Student','System','fakestudent@fake.com','stud3nt',group='student')
            self.keycloak.assign_realm_roles(uid,'student')
            log.warning('KEYCLOAK: OK')
        except:
            log.warning('KEYCLOAK: Seems to be there already')

        try:
            log.warning('NEXTCLOUD: Adding user ddadmin and adding to group admin')
            self.nextcloud.add_user(dduser,ddpassword,group='/admin',email=ddmail,displayname='DD Admin')
            log.warning('NEXTCLOUD: OK')
        except ProviderItemExists:
            log.warning('NEXTCLOUD: Seems to be there already')
        except:
            log.error(traceback.format_exc())
            exit(1)

        try:
            log.warning('MOODLE: Adding user ddadmin and adding to siteadmins')
            self.moodle.create_user(ddmail,dduser,ddpassword,'DD','Admin')
            uid=self.moodle.get_user_by('username',dduser)['users'][0]['id']
            self.moodle.add_user_to_siteadmin(uid)
            log.warning('MOODLE: OK')
        except:
            log.warning('MOODLE: Seems to be there already')

    def resync_data(self):
        self.internal={'users':self._get_mix_users(),
                        'groups':self._get_mix_groups(),
                        'roles':self._get_roles()}
        return True

    def get_moodle_users(self):
        return [u for u in self.moodle.get_users_with_groups_and_roles() if u['username'] not in ['guest','ddadmin','admin'] and not u['username'].startswith('system_')]

    ## TOO SLOW. Not used.
    # def get_moodle_users(self):
    #     log.warning('Loading moodle users... can take a long time...')
    #     users = self.moodle.get_users_with_groups_and_roles()
    #     #self.moodle.get_user_by('email','%%')['users']
    #     return [{"id":u['id'],
    #             "username":u['username'],
    #             "first": u['firstname'], 
    #             "last": u['lastname'], 
    #             "email": u['email'],
    #             "groups": u['groups'],
    #             "roles": u['roles']} 
    #             for u in users]

    def get_keycloak_users(self):
        # log.warning('Loading keycloak users... can take a long time...')
        users = self.keycloak.get_users_with_groups_and_roles()
        return [{"id":u['id'],
                "username":u['username'],
                "first": u.get('first_name',None), 
                "last": u.get('last_name',None), 
                "enabled": u['enabled'],
                "email": u.get('email',''),
                "groups": u['group'],
                "roles": filter_roles_list(u['role'])} 
                for u in users if u['username'] not in ['guest','ddadmin','admin'] and not u['username'].startswith('system')]

    def get_nextcloud_users(self):
        return [{"id":u['username'],
                "username":u['username'],
                "first": u['displayname'].split(' ')[0] if u['displayname'] is not None else '',
                "last": u['displayname'].split(' ')[1] if u['displayname'] is not None and len(u['displayname'].split(' '))>1 else '', 
                "email": u.get('email',''),
                "groups": u['groups'],
                "roles": False}
                for u in self.nextcloud.get_users_list() if u['username'] not in ['guest','ddadmin','admin'] and not u['username'].startswith('system')]

    ## TOO SLOW
    # def get_nextcloud_users(self):
    #     log.warning('Loading nextcloud users... can take a long time...')
    #     users = self.nextcloud.get_users_list()
    #     users_list=[]
    #     for user in users:
    #         u=self.nextcloud.get_user(user)
    #         users_list.append({"id":u['id'],
    #                         "username":u['id'],
    #                         "first": u['displayname'], 
    #                         "last": None, 
    #                         "email": u['email'],
    #                         "groups": u['groups'],
    #                         "roles": []})
    #     return users_list

    def get_mix_users(self):
        return self.internal['users']

    def _get_mix_users(self):
        kusers=self.get_keycloak_users()
        musers=self.get_moodle_users()
        nusers=self.get_nextcloud_users()

        kusers_usernames=[u['username'] for u in kusers]
        musers_usernames=[u['username'] for u in musers]
        nusers_usernames=[u['username'] for u in nusers]

        all_users_usernames=set(kusers_usernames+musers_usernames+nusers_usernames)

        users=[]
        for username in all_users_usernames:
            theuser={}
            keycloak_exists=[u for u in kusers if u['username'] == username]
            if len(keycloak_exists):
                theuser=keycloak_exists[0]
                theuser['keycloak']=True
                theuser['keycloak_groups']=self.keycloak.get_user_groups_paths(keycloak_exists[0]['id']) #keycloak_exists[0]['groups']
            else:
                theuser['id']=False
                theuser['keycloak']=False
                theuser['keycloak_groups']=[]

            moodle_exists=[u for u in musers if u['username'] == username]
            if len(moodle_exists):
                theuser={**moodle_exists[0], **theuser}
                theuser['moodle']=True
                theuser['moodle_groups']=moodle_exists[0]['groups']
                theuser['moodle_id']=moodle_exists[0]['id']
            else:
                theuser['moodle']=False
                theuser['moodle_groups']=[]

            nextcloud_exists=[u for u in nusers if u['username'] == username]
            if len(nextcloud_exists):
                theuser={**nextcloud_exists[0], **theuser}
                theuser['nextcloud']=True
                theuser['nextcloud_groups']=nextcloud_exists[0]['groups']
                theuser['nextcloud_id']=nextcloud_exists[0]['id']
            else:
                theuser['nextcloud']=False
                theuser['nextcloud_groups']=[]
            del theuser['groups']
            users.append(theuser)

        return users

    def get_roles(self):
        return self.internal['roles']

    def _get_roles(self):
        return filter_roles_listofdicts(self.keycloak.get_roles())

    def get_keycloak_groups(self):
        log.warning('Loading keycloak groups...')
        return self.keycloak.get_groups()

    def get_moodle_groups(self):
        log.warning('Loading moodle groups...')
        return self.moodle.get_cohorts()

    def get_nextcloud_groups(self):
        log.warning('Loading nextcloud groups...')
        return self.nextcloud.get_groups_list()

    def get_mix_groups(self):
        return self.internal['groups']

    def _get_mix_groups(self):
        kgroups=self.get_keycloak_groups() 
        mgroups=self.get_moodle_groups()
        ngroups=self.get_nextcloud_groups()

        kgroups=[] if kgroups is None else kgroups
        mgroups=[] if mgroups is None else mgroups
        ngroups=[] if ngroups is None else ngroups

        kgroups_names=[g['path'] for g in kgroups]
        mgroups_names=[g['name'] for g in mgroups]
        ngroups_names=ngroups

        all_groups_names=set(kgroups_names+mgroups_names+ngroups_names)

        groups=[]
        for name in all_groups_names:
            thegroup={}
            keycloak_exists=[g for g in kgroups if g['path'] == name]
            if len(keycloak_exists):
                thegroup=keycloak_exists[0]
                thegroup['keycloak']=True
                thegroup['path']=self.keycloak.get_group_path(keycloak_exists[0]['id'])
                del thegroup['subGroups']
            else:
                thegroup['id']=False
                thegroup['keycloak']=False

            moodle_exists=[g for g in mgroups if g['name'] == name]
            if len(moodle_exists):
                thegroup['path']=moodle_exists[0]['name']
                thegroup={**moodle_exists[0], **thegroup}
                thegroup['moodle']=True
                thegroup['moodle_id']=moodle_exists[0]['id']
            else:
                thegroup['moodle']=False

            nextcloud_exists=[g for g in ngroups if g == name]
            if len(nextcloud_exists):
                nextcloud={"id":nextcloud_exists[0],
                            "name":nextcloud_exists[0],
                            "path":nextcloud_exists[0]}
                thegroup={**nextcloud, **thegroup}
                thegroup['nextcloud']=True
                thegroup['nextcloud_id']=nextcloud_exists[0]  ### is the path
            else:
                thegroup['nextcloud']=False

            groups.append(thegroup)
        return groups

    def get_external_users(self):
        return self.external['users']

    def get_external_groups(self):
        return self.external['groups']

    def get_external_roles(self):
        return self.external['roles']

    def upload_csv_ug(self,data):
        log.warning('Processing uploaded users...')
        users=[]
        total=len(data['data'])
        item=1
        ev=Events('Processing uploaded users',total=len(data['data']))
        groups=[]
        for u in data['data']:
            log.warning('Processing ('+str(item)+'/'+str(total)+') uploaded user: '+u['username'])
            user_groups=["/" + g.strip() for g in u['groups'].split(',')]
            
            groups=groups+user_groups
            users.append({'provider':'external',
                'id':u['id'].strip(),
                'email': u['email'].strip(),
                'first': u['firstname'].strip(),
                'last': u['lastname'].strip(),
                'username': u['username'].strip(),
                'groups':user_groups,
                'roles':[u['role'].strip()],
                'password': self.get_dice_pwd()})
            item+=1
            ev.increment({'name':u['username'].split('@')[0]})
        self.external['users']=users

        groups=list(dict.fromkeys(groups))

        sysgroups=[]
        for g in groups:
            sysgroups.append({'provider':'external',
                            "id": g,
                            "mailid": g,
                            "name": g,
                            "description": 'Imported with csv'})
        self.external['groups']=sysgroups
        return True

    def get_dice_pwd(self):
        return diceware.get_passphrase(options=options)

    def reset_external(self):
        self.external={'users':[],
                        'groups':[],
                        'roles':[]}
        return True

    def upload_json_ga(self,data):
        groups=[]
        log.warning('Processing uploaded groups...')
        try:
            ev=Events('Processing uploaded groups','Group:',total=len(data['data']['groups']),table='groups')
        except:
            log.error(traceback.format_exc())
        for g in data['data']['groups']:
            try:
                group={'provider':'external',
                        "id": g['id'],
                        "mailid": g['email'].split('@')[0],
                        "name": g['name'],
                        "description": g['description']}
                ev.increment({'name':g['name'],'data':group})
                groups.append(group)
            except:
                pass
        self.external['groups']=groups

        log.warning('Processing uploaded users...')
        users=[]
        total=len(data['data']['users'])
        item=1
        ev=Events('Processing uploaded users','User:',total=len(data['data']['users']),table='users')
        for u in data['data']['users']:
            log.warning('Processing ('+str(item)+'/'+str(total)+') uploaded user: '+u['primaryEmail'].split('@')[0])
            new_user={'provider':'external',
                'id':u['id'],
                'email': u['primaryEmail'],
                'first': u['name']['givenName'],
                'last': u['name']['familyName'],
                'username': u['primaryEmail'].split('@')[0],
                'groups':[u['orgUnitPath']],  ## WARNING: Removing the first
                'roles':[],
                'password': self.get_dice_pwd()}
            users.append(new_user)
            item+=1
            ev.increment({'name':u['primaryEmail'].split('@')[0],'data':new_user})
        self.external['users']=users

        ## Add groups to users (now they only have their orgUnitPath)
        for g in self.external['groups']:
            for useringroup in data['data']['d_members'][g['mailid']]:
                for u in self.external['users']:
                    if u['id'] == useringroup['id']:
                        u['groups']=u['groups']+[g['name']]
        return True

    def sync_external(self,ids):
        log.warning('Starting sync to keycloak')
        self.sync_to_keycloak()
        ### Now we only sycn external to keycloak and then they can be updated to others with UI buttons
        log.warning('Starting sync to moodle')
        self.sync_to_moodle()
        log.warning('Starting sync to nextcloud')
        self.sync_to_nextcloud()
        log.warning('All syncs finished')

    def sync_to_keycloak(self):  ### This one works from the external, moodle and nextcloud from the internal
        groups=[]
        for u in self.external['users']:
            groups=groups+u['groups']
        groups=list(dict.fromkeys(groups))

        total=len(groups)
        i=0
        ev=Events('Syncing import groups to keycloak','Adding group:',total=len(groups))
        for g in groups:
            i=i+1
            log.warning(' KEYCLOAK GROUPS: Adding group ('+str(i)+'/'+str(total)+'): '+g)
            ev.increment({'name':g})
            self.keycloak.add_group_tree(g)

        total=len(self.external['users'])
        index=0
        ev=Events('Syncing import users to keycloak','Adding user:',total=len(self.external['users']))
        for u in self.external['users']:
            index=index+1
            # Add user
            log.warning(' KEYCLOAK USERS: Adding user ('+str(index)+'/'+str(total)+'): '+u['username'])
            ev.increment({'name':u['username'],'data':u})
            uid=self.keycloak.add_user(u['username'],u['first'],u['last'],u['email'],u['password'])
            self.av.add_user_default_avatar(uid,u['roles'][0])
            # Add user to role and group rolename
            if len(u['roles']) != 0:
                log.warning(' KEYCLOAK USERS: Assign user '+u['username']+' with initial pwd '+ u['password']+' to role '+u['roles'][0])
                self.keycloak.assign_realm_roles(uid,u['roles'][0])
                gid=self.keycloak.get_group_by_path(path='/'+u['roles'][0])['id']
                self.keycloak.group_user_add(uid,gid)
            # Add user to groups
            for g in u['groups']:
                parts=g.split('/')
                sub=''
                if len(parts)==0: 
                    log.warning(' KEYCLOAK USERS: Skip assign user '+u['username']+' to any group as does not have one')
                    continue # NO GROUP
                for i in range(1,len(parts)):
                    sub=sub+'/'+parts[i]
                    if sub=='/': continue # User with no path
                    log.warning(' KEYCLOAK USERS: Assign user '+u['username']+' to group '+ str(sub))
                    gid=self.keycloak.get_group_by_path(path=sub)['id']
                    self.keycloak.group_user_add(uid,gid)
        self.resync_data()

    def sync_to_moodle(self): # works from the internal (keycloak)
        ### Process all groups from the users keycloak_groups key
        groups=[]
        for u in self.internal['users']:
            groups=groups+u['keycloak_groups']
        groups=list(dict.fromkeys(groups))

        ### Create all groups. Skip / in system groups
        total=len(groups)
        i=0
        ev=Events('Syncing groups from keycloak to moodle',total=len(groups))
        for g in groups:
            parts=g.split('/')
            subpath=''
            for i in range(1,len(parts)):
                try:
                    log.warning(' MOODLE GROUPS: Adding group as cohort ('+str(i)+'/'+str(total)+'): '+subpath)
                    ev.increment({'name':subpath})
                    if parts[i] in ['admin','manager','teacher','student']:
                        subpath=parts[i]
                    else:
                        subpath=subpath+'/'+parts[i]
                    self.moodle.add_system_cohort(subpath)
                except:
                    log.error(' MOODLE GROUPS: Group '+subpath+ ' probably already exists')
            i=i+1
        
        ### Get all existing moodle cohorts
        cohorts=self.moodle.get_cohorts()

        ### Create users in moodle
        ev=Events('Syncing users from keycloak to moodle',total=len(self.internal['users']))
        for u in self.internal['users']:
            if not u['moodle']:
                log.warning('Creating moodle user: '+u['username'])
                ev.increment({'name':u['username']})
                if u['first'] == '': u['first']=' '
                if u['last'] == '': u['last']=' '
                try:
                    pprint(self.moodle.create_user(u['email'],u['username'],'1Random 1String',u['first'],u['last'])[0])
                except:
                    log.error('  -->> Error creating on moodle the user: '+u['username'])
                # user_id=user['id']

        self.resync_data()
        ### Add user to their cohorts (groups)
        ev=Events('Syncing users groups from keycloak to moodle cohorts',total=len(self.internal['users']))
        for u in self.internal['users']:
                total=len(u['keycloak_groups'])
                index=0
                ev.increment({'name':u['username']})
                for g in u['keycloak_groups']:
                    parts=g.split('/')
                    subpath=''
                    for i in range(1,len(parts)):
                        if parts[i] in ['admin','manager','teacher','student']:
                            subpath=parts[i]
                        else:
                            subpath=subpath+'/'+parts[i]

                        try:
                            cohort=[c for c in cohorts if c['name']==subpath][0]
                        except:
                            log.error(' MOODLE USER GROUPS: keycloak group '+subpath+' does not exist as moodle cohort. This should not happen. User '+u['username']+ ' not added.')

                        try:
                            self.moodle.add_user_to_cohort(u['moodle_id'],cohort['id'])
                        except:
                            log.error(' MOODLE USER GROUPS: User '+u['username']+' already exists in cohort '+cohort['name'])
                    index=index+1
        self.resync_data()

    def delete_all_moodle_cohorts(self):
        cohorts=self.moodle.get_cohorts()
        ids=[c['id'] for c in cohorts]
        self.moodle.delete_cohorts(ids)

    def sync_to_nextcloud(self):
        groups=[]
        for u in self.internal['users']:
            groups=groups+u['keycloak_groups']
        groups=list(dict.fromkeys(groups))

        total=len(groups)
        i=0
        ev=Events('Syncing groups from keycloak to nextcloud',total=len(groups))
        for g in groups:
            parts=g.split('/')
            subpath=''
            for i in range(1,len(parts)):
                try:
                    log.warning(' NEXTCLOUD GROUPS: Adding group ('+str(i)+'/'+str(total)+'): '+subpath)
                    ev.increment({'name':subpath})
                    subpath=subpath+'/'+parts[i]
                    self.nextcloud.add_group(subpath)
                except:
                    log.error('probably exists')
            i=i+1

        ev=Events('Syncing users from keycloak to nextcloud',total=len(self.internal['users']))
        for u in self.internal['users']:
            if not u['nextcloud']:
                log.warning('  NEXTCLOUD USERS: Creating nextcloud user: '+u['username']+' in groups '+str(u['keycloak_groups']))
                try:
                    ev.increment({'name':u['username']})
                    self.nextcloud.add_user_with_groups(u['username'],'1Random 1String',500000000000,u['keycloak_groups'],u['email'],u['first']+' '+u['last'])
                except ProviderItemExists:
                    log.warning('User '+u['username']+' already exists. Skipping...')
                    continue
                except:
                    log.error(traceback.format_exc())

    def delete_keycloak_user(self,userid):
        user=[u for u in self.internal['users'] if u['id']==userid]
        if len(user) and user[0]['keycloak']:
            user=user[0]
            keycloak_id=user['id']
        else:
            return False
        log.warning('Removing keycloak user: '+user['username'])
        try:
            self.keycloak.delete_user(keycloak_id)
        except:
            log.error(traceback.format_exc())
            log.warning('Could not remove users: '+user['username'])
        
        self.av.delete_user_avatar(userid)

    def delete_keycloak_users(self):
        total=len(self.internal['users'])
        i=0
        ev=Events('Deleting users from keycloak','Deleting user:',total=len(self.internal['users']))
        for u in self.internal['users']:
            i=i+1
            if not u['keycloak']: continue
            # Do not remove admin users!!! What to do with managers???
            if ['admin'] in u['roles']: continue
            log.info(' KEYCLOAK USERS: Removing user ('+str(i)+'/'+str(total)+'): '+u['username'])
            try:
                ev.increment({'name':u['username'],'data':u})
                self.keycloak.delete_user(u['id'])
            except:
                log.warning(' KEYCLOAK USERS: Could not remove user: '+u['username'] +'. Probably already not exists.')
        self.av.minio_delete_all_objects()

    def delete_nextcloud_user(self,userid):
        user=[u for u in self.internal['users'] if u['id']==userid]
        if len(user) and user[0]['nextcloud']:
            user=user[0]
            nextcloud_id=user['nextcloud_id']
        else:
            return False
        log.warning('Removing nextcloud user: '+user['username'])
        try:
            self.nextcloud.delete_user(nextcloud_id)
        except:
            log.error(traceback.format_exc())
            log.warning('Could not remove users: '+user['username'])

    def delete_nextcloud_users(self):
        ev=Events('Deleting users from nextcloud',total=len(self.internal['users']))
        for u in self.internal['users']:
            
            if u['nextcloud'] and not u['keycloak']:
                if u['roles'] and 'admin' in u['roles']: continue
                log.info('Removing nextcloud user: '+u['username'])
                try:
                    ev.increment({'name':u['username']})
                    self.nextcloud.delete_user(u['nextcloud_id']) 
                except:
                    log.error(traceback.format_exc())
                    log.warning('Could not remove user: '+u['username'])

    def delete_moodle_user(self,userid):
        user=[u for u in self.internal['users'] if u['id']==userid]
        if len(user) and user[0]['moodle']:
            user=user[0]
            moodle_id=user['moodle_id']
        else:
            return False
        log.warning('Removing moodle user: '+user['username'])
        try:
            self.moodle.delete_users([moodle_id])
        except:
            log.error(traceback.format_exc())
            log.warning('Could not remove users: '+user['username'])

    def delete_moodle_users(self):
        userids=[]
        usernames=[]
        for u in self.internal['users']:
            if u['moodle'] and not u['keycloak']:
                userids.append(u['moodle_id'])
                usernames.append(u['username'])
        if len(userids):
            log.warning('Removing moodle users: '+','.join(usernames))
            try:
                self.moodle.delete_users(userids)
                app.socketio.emit('update', 
                                json.dumps({'status':True,'item':'user','action':'delete','itemdata':u}), 
                                namespace='//sio', 
                                room='admin') 
            except:
                log.error(traceback.format_exc())
                log.warning('Could not remove users: '+','.join(usernames))



    def delete_keycloak_groups(self):
        for g in self.internal['groups']:
            if not g['keycloak']: continue
            # Do not remove admin group. It should not exist in keycloak, only in nextcloud
            if g['name'] in ['admin','manager','teacher','student']: continue
            log.info('Removing keycloak group: '+g['name'])
            try:
                self.keycloak.delete_group(g['id'])
            except:
                log.error(traceback.format_exc())
                log.warning('Could not remove group: '+g['name'])

    def external_roleassign(self,data):
        for newuserid in data['ids']:
            for externaluser in self.external['users']:
                if externaluser['id'] == newuserid:
                    externaluser['roles']=[data['action']]
        return True

    def user_update_password(self,userid,password,temporary):
        return self.keycloak.update_user_pwd(userid,password,temporary)

    def delete_user(self,userid):
        log.warning('deleting user moodle, nextcloud keycloak')
        ev=Events('Deleting user','Deleting from moodle')
        self.delete_moodle_user(userid)
        ev.update_text('Deleting from nextcloud')
        self.delete_nextcloud_user(userid)
        ev.update_text('Deleting from keycloak')
        self.delete_keycloak_user(userid)
        ev.update_text('Syncing data from applications...')
        self.resync_data()
        ev.update_text('User deleted')
        return True

    def get_user(self,userid):
        return [u for u in self.internal['users'] if u['id']==userid][0]
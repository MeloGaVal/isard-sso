#!/usr/bin/env python
# coding=utf-8
import time, os
from admin import app
from datetime import datetime, timedelta

import logging as log
import traceback
import yaml, json

import psycopg2

from .postgres import Postgres
# from .keycloak import Keycloak
# from .moodle import Moodle
import string, random

class Postup():
    def __init__(self):
        ready=False
        while not ready:
            try:
                self.pg=Postgres('isard-apps-postgresql','moodle',app.config['MOODLE_POSTGRES_USER'],app.config['MOODLE_POSTGRES_PASSWORD'])
                ready=True
            except:
                log.warning('Could not connect to moodle database. Retrying...')
                time.sleep(2)
        log.info('Connected to moodle database.')

        ready=False
        while not ready:
            try:
                with open(os.path.join(app.root_path, "../moodledata/saml2/moodle."+app.config['DOMAIN']+".crt"),"r") as crt:
                    app.config.setdefault('SP_CRT', crt.read())
                    ready=True
            except IOError:
                log.warning('Could not get moodle SAML2 crt certificate. Retrying...')
                time.sleep(2)
            except:
                log.error(traceback.format_exc())
        log.info('Got moodle srt certificate.')

        ready=False
        while not ready:
            try:
                with open(os.path.join(app.root_path, "../moodledata/saml2/moodle."+app.config['DOMAIN']+".pem"),"r") as pem:
                    app.config.setdefault('SP_PEM', pem.read())
                    ready=True
            except IOError:
                log.warning('Could not get moodle SAML2 pem certificate. Retrying...')
                time.sleep(2)
        log.info('Got moodle pem certificate.')

        self.select_and_configure_theme()
        self.configure_tipnc()
        self.add_moodle_ws_token()

    def select_and_configure_theme(self,theme='cbe'):
        try:
            self.pg.update("""UPDATE "mdl_config" SET value = '%s' WHERE "name" = 'theme';""" % (theme))
        except:
            log.error(traceback.format_exc())
            exit(1)
            None

        try:
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '%s' WHERE "plugin" = 'theme_cbe' AND "name" = 'host';""" % (os.environ['DOMAIN']))
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '%s' WHERE "plugin" = 'theme_cbe' AND "name" = 'logourl';""" % ("https://api."+os.environ['DOMAIN']+"/img/logo.png"))
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '1' WHERE "plugin" = 'theme_cbe' AND "name" = 'header_api';""")
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '1' WHERE "plugin" = 'theme_cbe' AND "name" = 'vclasses_direct';""")
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '1' WHERE "plugin" = 'theme_cbe' AND "name" = 'uniquenamecourse';""")
        except:
            log.error(traceback.format_exc())
            exit(1)
            None

    def configure_tipnc(self):
        try:
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '%s' WHERE "plugin" = 'assignsubmission_tipnc' AND "name" = 'host';""" % ("https://nextcloud."+os.environ['DOMAIN']+"/"))
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '%s' WHERE "plugin" = 'assignsubmission_tipnc' AND "name" = 'password';""" % (os.environ['NEXTCLOUD_ADMIN_PASSWORD']))
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = 'template.docx' WHERE "plugin" = 'assignsubmission_tipnc' AND "name" = 'template';""")
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '/apps/onlyoffice/' WHERE "plugin" = 'assignsubmission_tipnc' AND "name" = 'location';""")
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = '%s' WHERE "plugin" = 'assignsubmission_tipnc' AND "name" = 'user';""" % (os.environ['NEXTCLOUD_ADMIN_USER']))
            self.pg.update("""UPDATE "mdl_config_plugins" SET value = 'tasks' WHERE "plugin" = 'assignsubmission_tipnc' AND "name" = 'folder';""")
        except:
            log.error(traceback.format_exc())
            exit(1)
            None

    def add_moodle_ws_token(self):
        try:
            token=self.pg.select("""SELECT * FROM "mdl_external_tokens" WHERE "externalserviceid" = 3""")[0][1]
            app.config.setdefault('MOODLE_WS_TOKEN',token)
            return
        except:
            # log.error(traceback.format_exc())
            None

        try:
            self.pg.update("""INSERT INTO "mdl_external_services" ("name", "enabled", "requiredcapability", "restrictedusers", "component", "timecreated", "timemodified", "shortname", "downloadfiles", "uploadfiles") VALUES
            ('dd admin',	1,	'',	1,	NULL,	1621719763,	1621719850,	'dd_admin',	0,	0);""")

            self.pg.update("""INSERT INTO "mdl_external_services_functions" ("externalserviceid", "functionname") VALUES
            (3,	'core_course_update_courses'),
            (3,	'core_user_get_users'),
            (3,	'core_user_get_users_by_field'),
            (3,	'core_user_update_picture'),
            (3,	'core_user_update_users'),
            (3,	'core_user_delete_users'),
            (3,	'core_user_create_users'),
            (3,	'core_cohort_get_cohort_members'),
            (3,	'core_cohort_add_cohort_members'),
            (3,	'core_cohort_delete_cohort_members'),
            (3,	'core_cohort_create_cohorts'),
            (3,	'core_cohort_delete_cohorts'),
            (3,	'core_cohort_search_cohorts'),
            (3,	'core_cohort_update_cohorts'),
            (3,	'core_role_assign_roles'),
            (3,	'core_cohort_get_cohorts');""")

            self.pg.update("""INSERT INTO "mdl_external_services_users" ("externalserviceid", "userid", "iprestriction", "validuntil", "timecreated") VALUES
            (3,	2,	NULL,	NULL,	1621719871);""")

            b32=''.join(random.choices(string.ascii_uppercase + string.ascii_uppercase + string.ascii_lowercase, k = 32))
            b64=''.join(random.choices(string.ascii_uppercase + string.ascii_uppercase + string.ascii_lowercase, k = 64))
            self.pg.update("""INSERT INTO "mdl_external_tokens" ("token", "privatetoken", "tokentype", "userid", "externalserviceid", "sid", "contextid", "creatorid", "iprestriction", "validuntil", "timecreated", "lastaccess") VALUES
            ('%s',	'%s',	0,	2,	3,	NULL,	1,	2,	NULL,	0,	1621831206,	NULL);""" % (b32,b64))
            
            app.config.setdefault('MOODLE_WS_TOKEN',b32)
        except:
            log.error(traceback.format_exc())
            exit(1)
            None

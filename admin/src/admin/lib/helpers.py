
def filter_roles_list(role_list):
    client_roles=['admin','manager','teacher','student']
    return [r for r in role_list if r in client_roles]

def filter_roles_listofdicts(role_listofdicts):
    client_roles=['admin','manager','teacher','student']
    return [r for r in role_listofdicts if r['name'] in client_roles]
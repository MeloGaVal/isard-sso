#!/usr/bin/env python
# coding=utf-8

from admin import app

import os, sys
import logging as log
import traceback

class loadConfig():

    def __init__(self, app=None):  
        try:
            app.config.setdefault('DOMAIN', os.environ['DOMAIN'])
            app.config.setdefault('KEYCLOAK_POSTGRES_USER', os.environ['KEYCLOAK_DB_USER'])
            app.config.setdefault('KEYCLOAK_POSTGRES_PASSWORD', os.environ['KEYCLOAK_DB_PASSWORD'])
            app.config.setdefault('MOODLE_POSTGRES_USER', os.environ['MOODLE_POSTGRES_USER'])
            app.config.setdefault('MOODLE_POSTGRES_PASSWORD', os.environ['MOODLE_POSTGRES_PASSWORD'])
            app.config.setdefault('NEXTCLOUD_POSTGRES_USER', os.environ['NEXTCLOUD_POSTGRES_USER'])
            app.config.setdefault('NEXTCLOUD_POSTGRES_PASSWORD', os.environ['NEXTCLOUD_POSTGRES_PASSWORD'])
            app.config.setdefault('VERIFY', True if os.environ['VERIFY']=="true" else False)
        except Exception as e:
            log.error(traceback.format_exc())
            raise

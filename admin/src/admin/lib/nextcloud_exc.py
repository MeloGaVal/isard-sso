#!/usr/bin/env python
# coding=utf-8
class ProviderConnError(Exception):
    pass

class ProviderSslError(Exception):
    pass

class ProviderConnTimeout(Exception):
    pass

class ProviderError(Exception):
    pass

class ProviderItemExists(Exception):
    pass

class ProviderItemNotExists(Exception):
    pass

class ProviderGroupNotExists(Exception):
    pass

class ProviderFolderNotExists(Exception):
    pass

    
class ProviderOpError(Exception):
    pass

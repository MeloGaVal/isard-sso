#!/usr/bin/env python
# coding=utf-8

#from ..lib.log import *
from admin import app
import time,requests,json,pprint,os
import traceback
import logging as log
from .nextcloud_exc import *

from .postgres import Postgres

class Nextcloud():
    def __init__(self,
                url="https://nextcloud."+app.config['DOMAIN'],
                username=os.environ['NEXTCLOUD_ADMIN_USER'],
                password=os.environ['NEXTCLOUD_ADMIN_PASSWORD'],
                verify=True):

        self.verify_cert=verify
        self.apiurl=url+'/ocs/v1.php/cloud/'
        self.shareurl=url+'/ocs/v2.php/apps/files_sharing/api/v1/'
        self.davurl=url+'/remote.php/dav/files/'
        self.auth=(username,password)
        self.user=username

        self.nextcloud_pg=Postgres('isard-apps-postgresql','nextcloud',app.config['NEXTCLOUD_POSTGRES_USER'],app.config['NEXTCLOUD_POSTGRES_PASSWORD'])

    def _request(self,method,url,data={},headers={'OCS-APIRequest':'true'},auth=False):
        if auth == False: auth=self.auth
        try:
            return requests.request(method, url, data=data, auth=auth, verify=self.verify_cert, headers=headers).text

        ## At least the ProviderSslError is not being catched or not raised correctly
        except requests.exceptions.HTTPError as errh:
            raise ProviderConnError
        except requests.exceptions.Timeout as errt:
            raise ProviderConnTimeout
        except requests.exceptions.SSLError as err:
            raise ProviderSslError
        except requests.exceptions.ConnectionError as errc:
            raise ProviderConnError
        # except requests.exceptions.RequestException as err:
        #     raise ProviderError
        except Exception as e:
            if str(e) == 'an integer is required (got type bytes)':
                raise ProviderConnError
            raise ProviderError

    def check_connection(self):
        url = self.apiurl + "users/"+self.user+"?format=json"
        try:
            result = self._request('GET',url)
            if json.loads(result)['ocs']['meta']['statuscode'] == 100: return  True
            raise ProviderError
        except requests.exceptions.HTTPError as errh:
            raise ProviderConnError
        except requests.exceptions.ConnectionError as errc:
            raise ProviderConnError
        except requests.exceptions.Timeout as errt:
            raise ProviderConnTimeout
        except requests.exceptions.SSLError as err:
            raise ProviderSslError
        except requests.exceptions.RequestException as err:
            raise ProviderError
        except Exception as e:
            if str(e) == 'an integer is required (got type bytes)':
                raise ProviderConnError
            raise ProviderError

    def get_user(self,userid):
        url = self.apiurl + "users/"+userid+"?format=json"
        try:
            result = json.loads(self._request('GET',url))
            if result['ocs']['meta']['statuscode'] == 100: return  result['ocs']['data']
            raise ProviderItemNotExists
        except:
            log.error(traceback.format_exc())
            raise 
        # 100 - successful

# q = """select u.uid as username, adn.value as displayname, ade.value as email, json_agg(gg.displayname) as admin_groups,json_agg(g.displayname) as groups
# from oc_users as u 
# left join oc_group_user as gu on gu.uid = u.uid
# left join oc_groups as g on gu.gid = g.gid
# left join oc_group_admin as ga on ga.uid = u.uid
# left join oc_groups as gg on gg.gid = ga.gid
# left join oc_accounts_data as adn on adn.uid = u.uid and adn.name = 'displayname'
# left join oc_accounts_data as ade on ade.uid = u.uid and ade.name = 'email'
# group by u.uid, adn.value, ade.value"""
# cur.execute(q)
# users = cur.fetchall()
# fields = [a.name for a in cur.description]
# cur.close()
# conn.close()


# users_with_lists = [list(l[:-2])+([[]] if l[-2] == [None] else [list(set(l[-2]))]) + ([[]] if l[-1] == [None] else [list(set(l[-1]))]) for l in users]
# users_with_lists = [list(l[:-2])+([[]] if l[-2] == [None] else [list(set(l[-2]))]) + ([[]] if l[-1] == [None] else [list(set(l[-1]))]) for l in users_with_lists]
# list_dict_users = [dict(zip(fields, r)) for r in users_with_lists]
    def get_users_list(self):
        q = """select u.uid as username, adn.value as displayname, ade.value as email, json_agg(gg.displayname) as admin_groups,json_agg(g.displayname) as groups
                from oc_users as u 
                left join oc_group_user as gu on gu.uid = u.uid
                left join oc_groups as g on gu.gid = g.gid
                left join oc_group_admin as ga on ga.uid = u.uid
                left join oc_groups as gg on gg.gid = ga.gid
                left join oc_accounts_data as adn on adn.uid = u.uid and adn.name = 'displayname'
                left join oc_accounts_data as ade on ade.uid = u.uid and ade.name = 'email'
                group by u.uid, adn.value, ade.value"""
        (headers,users)=self.nextcloud_pg.select_with_headers(q)
        users_with_lists = [list(l[:-2])+([[]] if l[-2] == [None] else [list(set(l[-2]))]) + ([[]] if l[-1] == [None] else [list(set(l[-1]))]) for l in users]
        users_with_lists = [list(l[:-2])+([[]] if l[-2] == [None] else [list(set(l[-2]))]) + ([[]] if l[-1] == [None] else [list(set(l[-1]))]) for l in users_with_lists]
        list_dict_users = [dict(zip(headers, r)) for r in users_with_lists]
        return list_dict_users

    ### Too slow...
    # def get_users_list(self):
    #     url = self.apiurl + "users?format=json"
    #     try:
    #         result = json.loads(self._request('GET',url))
    #         if result['ocs']['meta']['statuscode'] == 100: return result['ocs']['data']['users']
    #         log.error('Get Nextcloud provider users list error: '+str(result))
    #         raise ProviderOpError
    #     except:
    #         log.error(traceback.format_exc())
    #         raise 

    def add_user(self,userid,userpassword,quota=False,group=False,email='',displayname=''):
        data={'userid':userid,'password':userpassword,'quota':quota,'groups[]':group,'email':email,'displayname':displayname}
        if not group: del data['groups[]']
        if not quota: del data['quota']
        # if group:
        #     data={'userid':userid,'password':userpassword,'quota':quota,'groups[]':group,'email':email,'displayname':displayname}
        # else:
        #     data={'userid':userid,'password':userpassword,'quota':quota,'email':email,'displayname':displayname}
        url = self.apiurl + "users?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('POST',url,data=data,headers=headers))
            if result['ocs']['meta']['statuscode'] == 100: return True
            if result['ocs']['meta']['statuscode'] == 102: raise ProviderItemExists
            if result['ocs']['meta']['statuscode'] == 104: 
                self.add_group(group)
                # raise ProviderGroupNotExists
            log.error('Get Nextcloud provider user add error: '+str(result))
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - invalid input data
            # 102 - username already exists
            # 103 - unknown error occurred whilst adding the user
            # 104 - group does not exist
            # 105 - insufficient privileges for group
            # 106 - no group specified (required for subadmins)
            # 107 - all errors that contain a hint - for example “Password is among the 1,000,000 most common ones. Please make it unique.” (this code was added in 12.0.6 & 13.0.1)

    def add_user_with_groups(self,userid,userpassword,quota=False,groups=[],email='',displayname=''):
        data={'userid':userid,'password':userpassword,'quota':quota,'groups[]':groups,'email':email,'displayname':displayname}
        # if not group: del data['groups[]']
        if not quota: del data['quota']
        # if group:
        #     data={'userid':userid,'password':userpassword,'quota':quota,'groups[]':group,'email':email,'displayname':displayname}
        # else:
        #     data={'userid':userid,'password':userpassword,'quota':quota,'email':email,'displayname':displayname}
        url = self.apiurl + "users?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('POST',url,data=data,headers=headers))
            if result['ocs']['meta']['statuscode'] == 100: return True
            if result['ocs']['meta']['statuscode'] == 102: raise ProviderItemExists
            if result['ocs']['meta']['statuscode'] == 104: 
                # self.add_group(group)
                None
                # raise ProviderGroupNotExists
            log.error('Get Nextcloud provider user add error: '+str(result))
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - invalid input data
            # 102 - username already exists
            # 103 - unknown error occurred whilst adding the user
            # 104 - group does not exist
            # 105 - insufficient privileges for group
            # 106 - no group specified (required for subadmins)
            # 107 - all errors that contain a hint - for example “Password is among the 1,000,000 most common ones. Please make it unique.” (this code was added in 12.0.6 & 13.0.1)

    def delete_user(self,userid):
        url = self.apiurl + "users/"+userid+"?format=json"
        try:
            result = json.loads(self._request('DELETE',url))
            if result['ocs']['meta']['statuscode'] == 100: return True
            if result['ocs']['meta']['statuscode'] == 101: raise ProviderUserNotExists
            log.error(traceback.format_exc())
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - failure

    def enable_user(self,userid):
        None

    def disable_user(self,userid):
        None

    def exists_user_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        url = self.davurl + userid +"/" + folder+"?format=json"
        headers = {
            'Depth': '0',
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = self._request('PROPFIND',url,auth=auth,headers=headers)
            if '<d:status>HTTP/1.1 200 OK</d:status>' in result: return True
            return False
        except:
            log.error(traceback.format_exc())
            raise 

    def add_user_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        url = self.davurl + userid +"/" + folder+"?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = self._request('MKCOL',url,auth=auth,headers=headers)
            if result=='': return True
            if '<s:message>The resource you tried to create already exists</s:message>' in result: raise ProviderItemExists
            log.error(result.split('message>')[1].split('<')[0])
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 

    def exists_user_share_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        url = self.shareurl + "shares?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result =  json.loads(self._request('GET', url, auth=auth, headers=headers))
            if result['ocs']['meta']['statuscode']==200:
                share=[s for s in result['ocs']['data'] if s['path'] == '/'+folder]
                if len(share) >= 1:
                    # Should we delete all but the first (0) one?
                    return {'token': share[0]['token'],
                            'url': share[0]['url']}
                raise ProviderItemNotExists
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 

    def add_user_share_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        data={'path':'/'+folder,'shareType':3}
        url = self.shareurl + "shares?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('POST',url, data=data, auth=auth, headers=headers))
            if result['ocs']['meta']['statuscode'] == 100 or result['ocs']['meta']['statuscode'] == 200:
                return {'token': result['ocs']['data']['token'],
                        'url': result['ocs']['data']['url']}
            log.error('Add user share folder error: '+result['ocs']['meta']['message'])
            raise ProviderFolderNotExists
        except:
            log.error(traceback.format_exc())
            raise 

    def get_group(self,userid):
        None

    def get_groups_list(self):
        url = self.apiurl + "groups?format=json"
        try:
            result = json.loads(self._request('GET',url))
            if result['ocs']['meta']['statuscode'] == 100: return  [g for g in result['ocs']['data']['groups']]
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 

    def add_group(self,groupid):
        data={'groupid':groupid} 
        url = self.apiurl + "groups?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('POST',url, data=data, auth=self.auth, headers=headers))
            if result['ocs']['meta']['statuscode'] == 100: return True
            if result['ocs']['meta']['statuscode'] == 102: raise ProviderItemExists
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - invalid input data
            # 102 - group already exists
            # 103 - failed to add the group

    def delete_group(self,groupid):
        url = self.apiurl + "groups/"+groupid+"?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('DELETE',url, auth=self.auth, headers=headers))
            if result['ocs']['meta']['statuscode'] == 100: return True
            log.error(traceback.format_exc())
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - invalid input data
            # 102 - group already exists
            # 103 - failed to add the group


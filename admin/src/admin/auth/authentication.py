from admin import app
from flask_login import LoginManager, UserMixin
import os

'''  OIDC TESTS '''
# from flask_oidc import OpenIDConnect
# app.config.update({
#     'SECRET_KEY': 'u\x91\xcf\xfa\x0c\xb9\x95\xe3t\xba2K\x7f\xfd\xca\xa3\x9f\x90\x88\xb8\xee\xa4\xd6\xe4',
#     'TESTING': True,
#     'DEBUG': True,
#     'OIDC_CLIENT_SECRETS': 'client_secrets.json',
#     'OIDC_ID_TOKEN_COOKIE_SECURE': False,
#     'OIDC_REQUIRE_VERIFIED_EMAIL': False,
#     'OIDC_VALID_ISSUERS': ['https://sso.santantoni.duckdns.org:8080/auth/realms/master'],
#     'OIDC_OPENID_REALM': 'https://sso.santantoni.duckdns.org//custom_callback',
#     'OVERWRITE_REDIRECT_URI': 'https://sso.santantoni.duckdns.org//custom_callback',
# })
#         # 'OVERWRITE_REDIRECT_URI': 'https://sso.santantoni.duckdns.org//custom_callback',
#         # 'OIDC_CALLBACK_ROUTE': '//custom_callback'
# oidc = OpenIDConnect(app)
'''  OIDC TESTS '''


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


ram_users={
  os.environ["ADMINAPP_USER"]: {
    'id': os.environ["ADMINAPP_USER"],
    'password': os.environ["ADMINAPP_PASSWORD"],
    'role': 'manager'
  },
  os.environ["KEYCLOAK_USER"]: {
    'id': os.environ["KEYCLOAK_USER"],
    'password': os.environ["KEYCLOAK_PASSWORD"],
    'role': 'admin',
  }
}

class User(UserMixin):
  def __init__(self, dict):
    self.id = dict['id']
    self.username = dict['id']
    self.password = dict['password']
    self.role = dict['role']

@login_manager.user_loader
def user_loader(username):
  return User(ram_users[username])

#!flask/bin/python
# coding=utf-8
from admin import app
import logging as log
import traceback

from uuid import uuid4
import time,json
import sys,os
from flask import render_template, Response, request, redirect, url_for, jsonify
import concurrent.futures
from flask_login import login_required
# import Queue
import threading
threads={}
# q = Queue.Queue()

from pprint import pprint

from keycloak.exceptions import KeycloakGetError

@app.route('/api/resync')
@login_required
def resync():
    return json.dumps(app.admin.resync_data()), 200, {'Content-Type': 'application/json'}

@app.route('/api/users', methods=['GET'])
@app.route('/api/users/<provider>', methods=['POST', 'PUT', 'GET', 'DELETE'])
@login_required
def users(provider=False):
    if request.method == 'DELETE':
        if provider == 'keycloak':
            return json.dumps(app.admin.delete_keycloak_users()), 200, {'Content-Type': 'application/json'}
        if provider == 'nextcloud':
            return json.dumps(app.admin.delete_nextcloud_users()), 200, {'Content-Type': 'application/json'}
        if provider == 'moodle':
            return json.dumps(app.admin.delete_moodle_users()), 200, {'Content-Type': 'application/json'}
    if request.method == 'POST':
        if provider == 'moodle':
            return json.dumps(app.admin.sync_to_moodle()), 200, {'Content-Type': 'application/json'}
        if provider == 'nextcloud':
            return json.dumps(app.admin.sync_to_nextcloud()), 200, {'Content-Type': 'application/json'}
    return json.dumps(app.admin.get_mix_users()), 200, {'Content-Type': 'application/json'}
    

# Update pwd
@app.route('/api/user_password', methods=['GET'])
@app.route('/api/user_password/<userid>', methods=['PUT'])
@login_required
def user_password(userid=False):
    if request.method == 'GET':
        return json.dumps(app.admin.get_dice_pwd()), 200, {'Content-Type': 'application/json'}
    if request.method == 'PUT':
        data=request.get_json(force=True)
        password=data['password']
        temporary=data.get('temporary',True)
        try:
            res = app.admin.user_update_password(userid,password,temporary)
            return json.dumps({}), 200, {'Content-Type': 'application/json'}
        except KeycloakGetError as e:
            print(e.error_message.decode("utf-8"))
            return e.error_message, e.response_code, {'Content-Type': 'application/json'}

    return json.dumps({}), 301, {'Content-Type': 'application/json'}

# User
@app.route('/api/user/<userid>', methods=['POST', 'PUT', 'GET', 'DELETE'])
@login_required
def user(userid=None):
    if request.method == 'DELETE':
        res = app.admin.delete_user(userid)
        return json.dumps({}), 200, {'Content-Type': 'application/json'}
        # return json.dumps(), 301, {'Content-Type': 'application/json'}
    if request.method == 'POST':
        pass
    if request.method == 'PUT':
        pass
    if request.method == 'DELETE':
        pass
    if request.method == 'GET':
        res = app.admin.get_user(userid)
        return json.dumps(res), 200, {'Content-Type': 'application/json'}

@app.route('/api/roles')
@login_required
def roles():
    sorted_roles = sorted(app.admin.get_roles(), key=lambda k: k['name'])
    return json.dumps(sorted_roles), 200, {'Content-Type': 'application/json'}


@app.route('/api/groups')
@app.route('/api/groups/<provider>', methods=['POST', 'PUT', 'GET', 'DELETE'])
@login_required
def groups(provider=False):
    if request.method == 'GET':
        sorted_groups = sorted(app.admin.get_mix_groups(), key=lambda k: k['name'])
        return json.dumps(sorted_groups), 200, {'Content-Type': 'application/json'}
    if request.method == 'DELETE':
        if provider == 'keycloak':
            return json.dumps(app.admin.delete_keycloak_groups()), 200, {'Content-Type': 'application/json'}



### SYSADM USERS ONLY

@app.route('/api/external', methods=['POST', 'PUT', 'GET','DELETE'])
@login_required
def external():
    if 'external' in threads.keys():
        if threads['external'] is not None and threads['external'].is_alive():
            return json.dumps({}), 301, {'Content-Type': 'application/json'}
        else:
            threads['external']=None

    if request.method == 'POST':
        data=request.get_json(force=True)
        if data['format']=='json-ga':
            threads['external'] = threading.Thread(target=app.admin.upload_json_ga, args=(data,))
            threads['external'].start()
            return json.dumps({}), 200, {'Content-Type': 'application/json'}
        if data['format']=='csv-ug':
            threads['external'] = threading.Thread(target=app.admin.upload_csv_ug, args=(data,))
            threads['external'].start()
            return json.dumps({}), 200, {'Content-Type': 'application/json'}
    if request.method == 'PUT':
        data=request.get_json(force=True)
        threads['external'] = threading.Thread(target=app.admin.sync_external, args=(data,))
        threads['external'].start()
        return json.dumps({}), 200, {'Content-Type': 'application/json'}
    if request.method == 'DELETE':
        print('RESET')
        app.admin.reset_external()
        return json.dumps({}), 200, {'Content-Type': 'application/json'}
    return json.dumps({}), 500, {'Content-Type': 'application/json'}

@app.route('/api/external/users')
@login_required
def external_users_list():
    return json.dumps(app.admin.get_external_users()), 200, {'Content-Type': 'application/json'}

@app.route('/api/external/groups')
@login_required
def external_groups_list():
    return json.dumps(app.admin.get_external_groups()), 200, {'Content-Type': 'application/json'}

@app.route('/api/external/roles', methods=['PUT'])
@login_required
def external_roles():
    if request.method == 'PUT':
        return json.dumps(app.admin.external_roleassign(request.get_json(force=True))), 200, {'Content-Type': 'application/json'}

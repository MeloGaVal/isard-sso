import os

from admin import app
from flask import render_template, flash, request, redirect, url_for
from ..auth.authentication import * 
from flask_login import login_required, current_user, login_user, logout_user

@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    if request.form['user'] == '' or request.form['password'] == '':
      flash("Can't leave it blank",'danger')
    elif request.form['user'].startswith(' '):
      flash('Username not found or incorrect password.','warning')
    else:
      ram_user=ram_users.get(request.form['user'])
      if ram_user and request.form['password'] == ram_user['password']:
        user=User({'id': ram_user['id'], 'password': ram_user['password'], 'role': ram_user['role'], 'active': True})
        login_user(user)
        flash('Logged in successfully.','success')
        return redirect(url_for('web_users'))
      else:
        flash('Username not found or incorrect password.','warning')
  return render_template('login.html')

@app.route('/logout', methods=['GET'])
@login_required
def logout():
  logout_user()
  return redirect(url_for('login'))

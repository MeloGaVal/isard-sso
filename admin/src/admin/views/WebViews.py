#!flask/bin/python
# coding=utf-8
from admin import app
import logging as log
import traceback

from uuid import uuid4
import time,json
import sys,os
from flask import render_template, Response, request, redirect, url_for, jsonify, send_file
import concurrent.futures
from flask_login import login_required

from pprint import pprint
from ..lib.avatars import Avatars

avatars=Avatars()

'''  OIDC TESTS '''
# from ..auth.authentication import oidc

# @app.route('/custom_callback')
# @oidc.custom_callback
# def callback(data):
#     return 'Hello. You submitted %s' % data

# @app.route('/private')
# @oidc.require_login
# def hello_me():
#     info = oidc.user_getinfo(['email', 'openid_id'])
#     return ('Hello, %s (%s)! <a href="/">Return</a>' %
#             (info.get('email'), info.get('openid_id')))


# @app.route('/api')
# @oidc.accept_token(True, ['openid'])
# def hello_api():
#     return json.dumps({'hello': 'Welcome %s' % g.oidc_token_info['sub']})


# @app.route('/logout')
# def logoutoidc():
#     oidc.logout()
#     return 'Hi, you have been logged out! <a href="/">Return</a>'
'''  OIDC TESTS '''

@app.route('/users')
@login_required
def web_users():
    return render_template('pages/users.html', title="Users", nav="Users")

@app.route('/roles')
@login_required
def web_roles():
    return render_template('pages/roles.html', title="Roles", nav="Roles")

@app.route('/groups')
@login_required
def web_groups(provider=False):
    return render_template('pages/groups.html', title="Groups", nav="Groups")

@app.route('/avatar/<userid>', methods=['GET'])
@login_required
def avatar(userid):
    if userid != 'false':
        return send_file('../avatars/master-avatars/'+userid, mimetype='image/jpeg')
    return send_file('static/img/missing.jpg', mimetype='image/jpeg')

### SYS ADMIN

@app.route('/sysadmin/users')
@login_required
def web_sysadmin_users():
    return render_template('pages/sysadmin/users.html', title="SysAdmin Users", nav="SysAdminUsers")

@app.route('/sysadmin/groups')
@login_required
def web_sysadmin_groups():
    return render_template('pages/sysadmin/groups.html', title="SysAdmin Groups", nav="SysAdminGroups")
    

@app.route('/sysadmin/external')
@login_required
## SysAdmin role
def web_sysadmin_external():
    return render_template('pages/sysadmin/external.html', title="External", nav="External")

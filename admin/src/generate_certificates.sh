cd saml_certs
C=CA
L=Barcelona
O=localdomain
CN_CA=$O
CN_HOST=*.$O
OU=$O
openssl req  -nodes -new -x509  -keyout private.key -out public.cert -subj "/C=$C/L=$L/O=$O/CN=$CN_CA"
cd ..
echo "Now run the python nextcloud script"
#!flask/bin/python
# coding=utf-8
from gevent import monkey
monkey.patch_all()

from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect, send
import json
from admin import app

app.socketio = SocketIO(app)

@app.socketio.on('connect', namespace='/sio')
def socketio_connect():
    join_room('admin')
    app.socketio.emit('update', 
                    json.dumps('Joined'), 
                    namespace='/sio', 
                    room='admin')
    
@app.socketio.on('disconnect', namespace='/sio')
def socketio_disconnect():
    None

if __name__ == '__main__':
    app.socketio.run(app,host='0.0.0.0', port=9000, debug=False, ssl_context='adhoc', async_mode="threading") #, logger=logger, engineio_logger=engineio_logger)
    # , cors_allowed_origins="*"
# /usr/lib/python3.8/site-packages/certifi
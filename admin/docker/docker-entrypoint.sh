#!/bin/sh
ssh-keygen -A
## Only in development
cd /admin/admin
yarn install
## End Only in development
cd /admin
export PYTHONWARNINGS="ignore:Unverified HTTPS request"
python3 start.py &
/usr/sbin/sshd -D -e -f /etc/ssh/sshd_config
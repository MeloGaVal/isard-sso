# IsardVDI OpenID infrastructure

All notable changes to this project will be documented in this file.

## [alpha1] - not released

### Added

- Auth containers: freeipa, mokey, hydra
- Configuration file: main.conf
- Build docker-compose yml: build.sh

### Fixed


### Changed


### Removed

